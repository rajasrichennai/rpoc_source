﻿using System;
using SkiaSharp;
using Xamarin.Forms;

namespace SkiaStudy
{
    public class LineDrawingFigure
    {
		SKPoint pt1, pt2;

		public long ID { get; private set; }

		public LineDrawingFigure()
		{
			ID = long.Parse(DateTime.Now.ToString("yyyyMMddHHmmssfff"));
		}
		public SKColor Color { set; get; }

		public SKPoint StartPoint
		{
			set
			{
				pt1 = value;
			}
		}

		public SKPoint EndPoint
		{
			set
			{
				pt2 = value;
				MakeLine();
			}
		}

		void MakeLine()
		{
			Line = new SKRect(pt1.X, pt1.Y, pt2.X, pt2.Y).Standardized;
		}

		public SKRect Line { set; get; }

		// For dragging operations
		public Point LastFingerLocation { set; get; }

		// For the dragging hit-test
		public bool IsInLine(SKPoint pt)
		{

            SKPath ss = new SKPath();
            ss.AddRect(new SKRect());
			SKRect rect = Line;

			return (Math.Pow(pt.X - rect.MidX, 2) / Math.Pow(rect.Width / 2, 2) +
					Math.Pow(pt.Y - rect.MidY, 2) / Math.Pow(rect.Height / 2, 2)) < 1;
		}

		public long ConnectedLineID = 0;
    }
}
