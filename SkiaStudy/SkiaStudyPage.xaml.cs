﻿using Xamarin.Forms;
using SkiaSharp;
using SkiaSharp.Views.Forms;
using System;
using System.Collections.Generic;
using TouchTracking;

using System.Linq;
using System.Diagnostics;
namespace SkiaStudy
{
    public partial class SkiaStudyPage : ContentPage
    {
        
		List<object> completedFigures = new List<object>();
        Dictionary<long, object> draggingFigures = new Dictionary<long, object>();

		Random random = new Random();
        SKPaint blackFillPaint = new SKPaint()
        {
            Style = SKPaintStyle.Fill,
            Color = SKColors.Black
        };

        SKPaint redFillPaint = new SKPaint()
        {
            Style = SKPaintStyle.Fill,
            Color = SKColors.Red
        };


        SKPaint whiteStrokePaint = new SKPaint
        {
            Style = SKPaintStyle.Stroke,
            Color = SKColors.White,
            StrokeWidth = 2f,
            StrokeCap = SKStrokeCap.Round,
            IsAntialias = true
        };

        SKPaint whiteCircleFill = new SKPaint
        {
            Style = SKPaintStyle.Fill,
            Color = SKColors.White
        };

        SKPath catEarPath = new SKPath();
        SKPath catEyePath = new SKPath();
        SKPath catPupilPath = new SKPath();
        SKPath catTailPath = new SKPath();
        public SkiaStudyPage()
        {
            InitializeComponent();


            //Make Cat ear Path
            catEarPath.MoveTo(0, 0);
            catEarPath.LineTo(0, 75);
            catEarPath.LineTo(75, 100);
            catEarPath.Close();


        }


		void OnClearButtonClicked(object sender, EventArgs args)
		{
			completedFigures.Clear();
   //         scale -= 1;
			canvasView.InvalidateSurface();
		}




		void OnTouchEffectAction(object sender, TouchActionEventArgs args)
		{
            //allow singel touch
            if (args.Id != 0)
                return;
			switch (args.Type)
			{
				case TouchActionType.Pressed:
					bool isDragOperation = false;

					// Loop through the completed figures
                    foreach (object fig in completedFigures.Reverse<object>())
					{
                        if (fig is RectangleDrawingFigure)
                        {
                            // Check if the finger is touching one of the ellipses
                            if ((fig as RectangleDrawingFigure).IsInRectangle(ConvertToPixel(args.Location)))
                            {
                                // Tentatively assume this is a dragging operation
                                isDragOperation = true;

                                // Loop through all the figures currently being dragged
                                foreach (RectangleDrawingFigure draggedFigure in draggingFigures.Values)
                                {
                                    // If there's a match, we'll need to dig deeper
                                    if (fig == draggedFigure)
                                    {
                                        isDragOperation = false;
                                        break;
                                    }
                                }

                                if (isDragOperation)
                                {
                                    (fig as RectangleDrawingFigure).LastFingerLocation = args.Location;
                                    draggingFigures.Add(args.Id, fig);
                                    break;
                                }
                            }
                        }else if (fig is LineDrawingFigure)
						{
							// Check if the finger is touching one of the ellipses
                            if ((fig as LineDrawingFigure).IsInLine(ConvertToPixel(args.Location)))
							{
								// Tentatively assume this is a dragging operation
								isDragOperation = true;

								// Loop through all the figures currently being dragged
								foreach (LineDrawingFigure draggedFigure in draggingFigures.Values)
								{
									// If there's a match, we'll need to dig deeper
									if (fig == draggedFigure)
									{
										isDragOperation = false;
										break;
									}
								}

								if (isDragOperation)
								{
									(fig as LineDrawingFigure).LastFingerLocation = args.Location;
									draggingFigures.Add(args.Id, fig);
									break;
								}
							}
						}

					}

					if (isDragOperation)
					{
						// Move the dragged ellipse to the end of completedFigures so it's drawn on top
                        object fig = draggingFigures[args.Id];
						completedFigures.Remove(fig);
						completedFigures.Add(fig);
					}
					canvasView.InvalidateSurface();
					break;

				case TouchActionType.Moved:
					if (draggingFigures.ContainsKey(args.Id))
					{
                        if (draggingFigures[args.Id] is RectangleDrawingFigure)
                        {
                            RectangleDrawingFigure figure = draggingFigures[args.Id] as RectangleDrawingFigure;
                            SKRect rect = figure.Rectangle;
                            rect.Offset(ConvertToPixel(new Point(args.Location.X - figure.LastFingerLocation.X,
                                                                 args.Location.Y - figure.LastFingerLocation.Y)));
                            figure.Rectangle = rect;
                            figure.LastFingerLocation = args.Location;
                        }
                        else if (draggingFigures[args.Id] is LineDrawingFigure)
                        {
							LineDrawingFigure figure = draggingFigures[args.Id] as LineDrawingFigure;
                            SKRect rect = figure.Line;
							rect.Offset(ConvertToPixel(new Point(args.Location.X - figure.LastFingerLocation.X,
																 args.Location.Y - figure.LastFingerLocation.Y)));
                            figure.Line = rect;
							figure.LastFingerLocation = args.Location;
                        }
					}
					canvasView.InvalidateSurface();
					break;

				case TouchActionType.Released:
					if (draggingFigures.ContainsKey(args.Id))
					{

                        //This is to check whether it is connected to other or not
                        if (draggingFigures[args.Id] is RectangleDrawingFigure && (draggingFigures[args.Id] as RectangleDrawingFigure).ConnectedLineID == 0)
                        {
                            RectangleDrawingFigure rect = (draggingFigures[args.Id] as RectangleDrawingFigure);
                            Debug.WriteLine(rect.ID +"  is not connected");
                            foreach (object fig in completedFigures)
                            {
                                if (fig is LineDrawingFigure && (fig as LineDrawingFigure).ConnectedLineID == 0){
                                    if(IsLineInRectangle((fig as LineDrawingFigure), rect)){
                                        Debug.WriteLine("Is going to connected with Rec");
										LineDrawingFigure figure = fig as LineDrawingFigure;
										SKRect lineRect = figure.Line;
                                        lineRect.Top = rect.Rectangle.Bottom;
                                        lineRect.Left = rect.Rectangle.Left + (rect.Rectangle.Width / 2);
                                        lineRect.Bottom = lineRect.Top + (figure.Line.Bottom - figure.Line.Top);
                                        lineRect.Right = lineRect.Left + figure.Line.Width;
										figure.Line = lineRect;
										figure.LastFingerLocation = args.Location;
                                    }
                                }
                            }
                        }
						draggingFigures.Remove(args.Id);
					}

					canvasView.InvalidateSurface();
					break;

				case TouchActionType.Cancelled:
					if (draggingFigures.ContainsKey(args.Id))
					{
						draggingFigures.Remove(args.Id);
					}
					canvasView.InvalidateSurface();
					break;
			}

			
		}

		
        void canvasView_PaintSurface(object sender, SKPaintSurfaceEventArgs args)
        {
            SKCanvas canvas = args.Surface.Canvas;
            canvas.Clear();

            for (int i = 50; i < args.Info.Width; i+=50){
                canvas.DrawLine(i,0,i,args.Info.Height,BlueStrokePaint);
            }

            for (int i = 50; i < args.Info.Height; i+=50){
                canvas.DrawLine(0,i,args.Info.Width,i,BlueStrokePaint);
            }
            foreach (object figure in completedFigures)
			{
                if (figure is RectangleDrawingFigure)
                {
                    paint.Color = (figure as RectangleDrawingFigure).Color;
                    canvas.DrawRect((figure as RectangleDrawingFigure).Rectangle, paint);
                }else if (figure is LineDrawingFigure)
				{
					paint.Color = (figure as LineDrawingFigure).Color;
                    canvas.DrawRect((figure as LineDrawingFigure).Line, paint);
                }

			}
		}

		SKPaint paint = new SKPaint
		{
			Style = SKPaintStyle.Fill
		};

		SKPaint BlueStrokePaint = new SKPaint
		{
            Color = SKColors.Blue,
            Style = SKPaintStyle.Stroke
		};


		SKPoint ConvertToPixel(Point pt)
		{
			return new SKPoint((float)(canvasView.CanvasSize.Width * pt.X / canvasView.Width),
							   (float)(canvasView.CanvasSize.Height * pt.Y / canvasView.Height));
		}

        void OnClickAddRect(object sender, EventArgs e){
			byte[] buffer = new byte[4];
			random.NextBytes(buffer);
			RectangleDrawingFigure figure = new RectangleDrawingFigure
			{
                // Random bytes for random color
                      
				Color = new SKColor(buffer[0], buffer[1], buffer[2], buffer[3]),
                StartPoint = new SKPoint(0,0),
				EndPoint = new SKPoint(250, 250)
			};
            completedFigures.Add(figure);
            canvasView.InvalidateSurface();
        }

        void OnClickAddLine(object sender, EventArgs e)
        {
			byte[] buffer = new byte[4];
			random.NextBytes(buffer);
            LineDrawingFigure figure = new LineDrawingFigure
			{
				// Random bytes for random color

				Color = new SKColor(buffer[0], buffer[1], buffer[2], buffer[3]),
				StartPoint = new SKPoint(50, 50),
				EndPoint = new SKPoint(70, 500)
			};
			completedFigures.Add(figure);
			canvasView.InvalidateSurface();
            canvasView.InvalidateSurface();
        }

         bool IsLineInRectangle(LineDrawingFigure line,RectangleDrawingFigure Rect)
		{
                                        
            SKPoint pt = new SKPoint(line.Line.Left, line.Line.Top);
                                        SKRect rect = new SKRect(Rect.Rectangle.Left,Rect.Rectangle.Bottom,Rect.Rectangle.Right,Rect.Rectangle.Bottom+50);

			return (Math.Pow(pt.X - rect.MidX, 2) / Math.Pow(rect.Width / 2, 2) +
					Math.Pow(pt.Y - rect.MidY, 2) / Math.Pow(rect.Height / 2, 2)) < 1;
		}
    }
}
